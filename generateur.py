from tkinter import *
import string

#decoupe la chaine passe en parametre pour recuperer le nom et le prenom
def decoupageNom(nomComplet):
    mots = nomComplet.split()
    return mots

#renvoir l'index correspondant a un caractere passee en parametre
def getIndexChar(character):
    alphabet = string.ascii_uppercase
    character = character.upper()
    return alphabet.index(character)

#renvoir le nom correspondant a celui passe en parametre par rapport a un univers fictif lui aussi passe en parametre
def getCorrespondance(nomComplet, univers):
    mots = decoupageNom(nomComplet)
    if(len(mots)!=2):
        return "Erreur la Saisie ne correspond pas a un nom"
    try:
        nom = univers["listLastname"][getIndexChar(mots[1][univers["lastnameLetterPosition"]-1])]
        prenom = univers["listFirstname"][getIndexChar(mots[0][univers["firstnameLetterPosition"]-1])]
        nomCorrespondant = "%s %s" % (nom, prenom)
    except:
        nomCorrespondant = "Erreur un des noms est trop court"
    return nomCorrespondant

def callback_getNom():
    nomCorrespondant = getCorrespondance(saisie_nom.get().strip(), univers)
    lbl_nomfictiftxt.set(nomCorrespondant)

def callback_starwars():
    global univers 
    univers = starwars_dictionnary
    lbl_saisietxt.set("Saisissez votre nom et decouvrez votre nom\nde l'univers Star Wars : ")

def callback_elder_scrolls():
    global univers 
    univers = elder_scrolls_dictionnary
    lbl_saisietxt.set("Saisissez votre nom et decouvrez votre nom\nde l'univers The Elder Scrolls : ")

def callback_mlp():
    global univers 
    univers = mlp_dictionnary
    lbl_saisietxt.set("Saisissez votre nom et decouvrez votre nom\nde l'univers My Little Pony : ")


if __name__ == '__main__':

    #Star wars dictionnaire de config
    starwars_listlastname = ["Darth","Padame","Jar Jar", "Obi Wan", "Emperor", "Han", "Greedo", "Anakin", "Count", "Sarlacc", 
                        "Jabba", "Sergeant", "Boba", "Wicket W", "Wampa", "Senator", "Queen", "Padawan", "Imperial", "General", 
                        "Storm", "R2", "Luke", "C3", "Mace", "Princess"]
    starwars_listfirstname = ["Bane", "Sidious", "Skywalker", "Chewbaca", "Fett", "Trooper", "PO", "Speeder", "Dooku", "Pit", "Vader",
                        "DZ", "Palpatine", "Grievous", "Binks", "Soldier", "Kenobi", "Solo", "The Hutt", "Maul", "Windu", "Queen", 
                        "Leia", "Monster", "Yoda", "Warrick"]

    starwars_dictionnary = {"listLastname":starwars_listlastname, "listFirstname": starwars_listfirstname, 
                            "lastnameLetterPosition": 1, "firstnameLetterPosition": 2}

    univers = starwars_dictionnary

    #mlp dictionnaire de config
    mlp_listlastname = ["Shining", "Magical", "Sparkling", "Unicorn", "Mane", "Glitter", "Moonbeam", "Unicorn", "Blossom", 
                    "Princess", "Golden", "Cupcake", "Marshmallow", "Sunshine", "Star", "Dreamy", "Precious", "Cakepop", 
                    "Lovely", "Twirly", "Fancy", "Buttercup", "Pretty", "Strawberry", "Confetti", "Sweetie"]
    mlp_listfirstname = ["Angel Mane", "Rainbow Trix", "Sparkle Socks", "Candy Cane", "Glitterhoofs", "Goldencloud", "Bubblegum", 
                    "Lilyshine", "Daisy Fairy", "Vanilla Angel", "Tiny Dancer", "Shimmer Rain", "Diamondsparkle", "Tuttifruti", 
                    "Moon Dance", "Sunshine", "Happyhoofs", "Banana Bubbles", "Prancershine", "Destiny", "Funshine", "Twinkletoes", 
                    "Gumdrop", "Majestic Bonbon", "Tiara CandyFizz", "Sugarkiss Sparkle"]
    mlp_dictionnary = {"listLastname":mlp_listlastname, "listFirstname": mlp_listfirstname, 
                            "lastnameLetterPosition": 2, "firstnameLetterPosition": 3}

    #elder_scrolls dictionnaire de config
    elder_scrolls_listlastname = ["Armandre", "Balreth", "Claws-In-Gloves", "Dumae", "Edwyn", "Freia", "Gruzgom", "Hellex", 
                                "Irarak", "Jurak-dar", "Kalanan", "Lillandra", "Mikael", "Niro", "Orgnum", "Perthan", "Qusan", 
                                "Radithax", "Silvan", "Tahar-dar", "Uldun", "Verick", "Wynaldia", "Akatosh", "Jyggalag", "Peryite"]
    elder_scrolls_listfirstname = ["The All-Knowing", "The Beast", "The Cute", "The Divine", "The Enchanter", "The Fungusfish", 
                                "The Grizzly", "The Hero", "The Infamous", "The Jackal", "The Knight", "The Lover", 
                                "The Mastermind", "The Nocturnal", "The OverPowered", "The Powerful", "The Quick", "The Rascal", 
                                "The Serpent", "The Thief", "The Undiying", "The Vampire", "The Werewolf", "The Tyrant", "The Dumb",
                                 "The Insane"]
    elder_scrolls_dictionnary = {"listLastname":elder_scrolls_listlastname, "listFirstname": elder_scrolls_listfirstname, 
                            "lastnameLetterPosition": 2, "firstnameLetterPosition": 1}




    #Configuration de la fenetre
    frame = Tk()
    frame.title("Generateur de Noms")
    frame.minsize(600, 250)
    frame.configure(background='white')

    #Configuration de la grille de la fenetre
    frame.rowconfigure(0, weight=1)
    frame.rowconfigure(1, weight=1)
    frame.rowconfigure(2, weight=1)
    frame.rowconfigure(3, weight=1)
    frame.columnconfigure(0, weight=1)
    frame.columnconfigure(1, weight=1)
    frame.columnconfigure(2, weight=1)

    #Bouton choix univers star wars
    btn_starwars = Button(frame,
                            width = 18, 
                            text='Star Wars',
                            anchor = 'center',
                            command=callback_starwars)

    btn_starwars.grid(row=0, column=0)

    #Bouton choix univers mlp
    btn_mlp = Button(frame,
                            width = 18, 
                            anchor = 'center',
                            text='My little pony',
                            command=callback_mlp)

    btn_mlp.grid(row=0, column=1)

    #Bouton choix univers elder scrolls
    btn_elder_scrolls = Button(frame,
                            width = 18, 
                            anchor = 'center',
                            text='The Elder Scrolls',
                            command=callback_elder_scrolls)

    btn_elder_scrolls.grid(row=0, column=2)

    #label d'instructions
    lbl_saisietxt = StringVar()
    lbl_saisietxt.set("Saisissez votre nom et découvrez votre nom\nde l'univers Star Wars : ")
    lbl_saisie = Label(frame, 
                        textvariable = lbl_saisietxt, 
                        bg = 'white')
    lbl_saisie.grid(row=1, column=1)

    #entry saisie du nom
    saisie_nom = Entry(frame, 
                        width=25,
                        bg='#EEEEEE')
    saisie_nom.grid(row=2, column=1)
    
    #Bouton decouvrir le nom
    btn_getnom = Button(frame,
                            width = 17, 
                            anchor = 'center',
                            text='Decouvrir votre nom',
                            command=callback_getNom,)

    btn_getnom.grid(row=2, column=2)

    #label afichage du nom obtenu
    lbl_nomfictiftxt = StringVar()
    lbl_nomfictiftxt.set("...")
    lbl_nomfictif = Label(frame, 
                        textvariable = lbl_nomfictiftxt, 
                        bg = 'white')
    lbl_nomfictif.grid(row=3, column=1)

    frame.mainloop()
