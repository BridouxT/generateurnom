import unittest
import string
from generateur import decoupageNom, getIndexChar, getCorrespondance

class TestGenerateur(unittest.TestCase):

    def test_decoupageNom(self):
        mots = ["Thomas", "Bridoux"]
        self.assertEqual(mots, decoupageNom("Thomas Bridoux"))
        mots = ["a", "Z"]
        self.assertEqual(mots, decoupageNom("a Z"))

    def test_indexAlphabet(self):
        self.assertEqual(getIndexChar("a"), 0)
        self.assertEqual(getIndexChar("A"), 0)
        self.assertEqual(getIndexChar("b"), 1)
        self.assertEqual(getIndexChar("Z"), 25)

    def test_getCorrespondance(self):
        #dictionnaire de config
        starwars_listlastname = ["Darth","Padamé","Jar Jar", "Obi Wan", "Emperor", "Han", "Greedo", "Anakin", "Count", "Sarlacc", 
                        "Jabba", "Sergeant", "Boba", "Wicket W", "Wampa", "Senator", "Queen", "Padawan", "Imperial", "General", 
                        "Storm", "R2", "Luke", "C3", "Mace", "Princess"]
        starwars_listfirstname = ["Bane", "Sidious", "Skywalker", "Chewbaca", "Fett", "Trooper", "PO", "Speeder", "Dooku", "Pit", "Vader",
                        "DZ", "Palpatine", "Grievous", "Binks", "Soldier", "Kenobi", "Solo", "The Hutt", "Maul", "Windu", "Queen", 
                        "Leia", "Monster", "Yoda", "Warrick"]

        starwars_dictionnary = {"listLastname":starwars_listlastname, "listFirstname": starwars_listfirstname, 
                            "lastnameLetterPosition": 1, "firstnameLetterPosition": 2}

        univers = starwars_dictionnary

        self.assertEqual(getCorrespondance("Thomas Bridoux", univers), "Padame Speeder")
        self.assertEqual(getCorrespondance("aaaaaaaa aaaaaaaaaaaa", univers), "Darth Bane")
        self.assertEqual(getCorrespondance("ZZZZZZZZZZ ZZZZZZZZZZ", univers), "Princess Warrick")
        self.assertEqual(getCorrespondance("T T T", univers), "Erreur la Saisie ne correspond pas à un nom")
        self.assertEqual(getCorrespondance("T", univers), "Erreur la Saisie ne correspond pas à un nom")
        self.assertEqual(getCorrespondance("T T", univers), "Erreur un des noms est trop court")
